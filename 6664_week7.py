# -*- coding: utf-8 -*-
"""6664_week7

Automatically generated by Colaboratory.

Original file is located at
    https://colab.research.google.com/drive/1RCCk-axJMnCxoAhCjCSwE56t-GWMObQO
"""

#1.use map to create a new list by changing each country to uppercase in the country list
countries=["india","pakisthan","usa","canada"]
def fn(x):
  return x.upper()
y=map(fn,countries)
print(list(y))

#2.Use filter to filter out countries having exactly six characters
countries=["india","pakisthan","usa","canada"]
def fn(y):
  if len(y)==6:
    return y
z=list(filter(fn,countries))
print(list(z))

#3.create a function returning a dictionary where keys stand for starting letters of country and values number of country names starting with that letter
c=0
max=0
y=0
for i in range(3,10):
 c=0
 for j in range(1,1001):
   if j%i==0:
    c+=1
 print("The number of factors of {} is {}".format(i,c))
 if c>max:
   max=c
   y=i
print("The number having highest multiples betwwen3-9 is",y)

4.#.for all the numbers 1-1000 use a nested list comprehension to find how many number by each single digit 3-9 and print single digit which has highest number of multiples
def dictn(countries):
  d={}
  for i in countries:
    m=0
    for k in countries:
      if(i[0]==k[0]):
        m+=1
        d[i[0]]=m
  return d
countries=['india','ireland','chile','pakisthan','uganda','china']
result=dictn(countries)
print(result)